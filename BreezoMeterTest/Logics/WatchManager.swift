//
//  WatchManager.swift.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import Foundation

enum Mode {
    case clock
    case alarm
    case stopwatch
}


enum SetMode {
    case none
    case hour
    case min
    case sec
}

class WatchManager: NSObject, WatchMainVCDelegate {
    weak var watchVC: WatchMainVC?
    var watchObject = WatchObject()
    var timeObject = TimeObject()
    var alarmObject = AlarmObject()
    var stopwatchObject = StopwatchObject()
    var clockTimer: Timer?
    var stopwatchTimer: Timer?
    let calendar = Calendar.current
    let soundPlayer = SoundPlayer()
    var startSetPress: Date?
    var alarmSoundSeconds = 0
    
    //Initializing WatchManager//
    @objc func start(notification: Notification) {
        //Get watch UI from notification when UI is ready//
        self.watchVC = notification.object as? WatchMainVC
        
        //Creating time delta so that the watch starts at 00:00:00//
        updateTimeObject(delta: 0)
        watchObject.localGMTdelta = TimeInterval(-1 * ((timeObject.hour*60*60) + (timeObject.min*60) + timeObject.sec))
        
        //Start timer for clock and set time flicker affect//
        clockTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.timeChanged), userInfo: nil, repeats: true)
        clockTimer?.fire()
        
        //Update alarm status//
        self.watchVC?.setAlarm(on: alarmObject.alarmOn)
        
        self.watchVC?.delegate = self
    }
    
    //Called when tapped light button//
    func watchLightTapped() {
        if alarmSoundSeconds > 0 {
            stopAlarmSound(snooze: true)
        } else {
            soundPlayer.playSound(type: .beep)
            if let watchVC = self.watchVC {
                watchVC.light(on: true)
                NSObject.cancelPreviousPerformRequests(withTarget: watchVC, selector: #selector(watchVC.light(on:)), object: false)
                watchVC.perform(#selector(watchVC.light(on:)), with: false, afterDelay: TimeInterval(watchObject.backlightSec))
            }
        }
    }
    
    //Called when tapped set button//
    func watchSetStartedTapped() {
        stopAlarmSound(snooze: false)
        soundPlayer.playSound(type: .beep)
        if watchObject.currentMode != .stopwatch {
            startSetPress = Date()
        }
    }
    
    //Called when released set button//
    func watchSetEndedTapped() {
        if startSetPress != nil {
            startSetPress = nil
            if watchObject.currentSetMode == .hour {
                setCurrentSetMode(mode: .min)
            } else if watchObject.currentSetMode == .min {
                if watchObject.currentMode == .clock {
                    setCurrentSetMode(mode: .sec)
                } else {
                    setCurrentSetMode(mode: .hour)
                }
            } else if watchObject.currentSetMode == .sec {
                setCurrentSetMode(mode: .hour)
            }
        }
    }
    
    //Called when tapped up button//
    func WatchUpTapped() {
        soundPlayer.playSound(type: .beep)
        if watchObject.currentMode == .stopwatch {
            if watchObject.stopwatchPaused && !watchObject.stopwatchLap {//Reset Stopwatch
                watchObject.stopwatchGMTdelta = 0
                stopwatchObject.reset()
                updateStopwatchUI()
            } else {//Lap
                watchObject.stopwatchLap = !watchObject.stopwatchLap
                updateStopwatchUI()
            }
        } else {
            stopAlarmSound(snooze: false)
            addSubTime(up: true)
        }
    }
    
    //Called when tapped down button//
    func watchDownTapped() {
        soundPlayer.playSound(type: .beep)
        if watchObject.currentMode == .stopwatch {
            startPauseStapwatch()
        } else {
            stopAlarmSound(snooze: false)
            addSubTime(up: false)
        }
    }
    
    func startPauseStapwatch() {
        if watchObject.stopwatchPaused {
            watchObject.stopwatchPaused = false
            let prev = TimeInterval((Double(stopwatchObject.hour)*60*60) + (Double(stopwatchObject.min)*60) + Double(stopwatchObject.sec) + (Double(stopwatchObject.mili) / 100))
            updateStopwatchObject(delta: 0)
            watchObject.stopwatchGMTdelta = TimeInterval(-1 * ((stopwatchObject.hour*60*60) + (stopwatchObject.min*60) + stopwatchObject.sec + stopwatchObject.mili/100)) + prev
            print("watchObject.stopwatchGMTdelta = \(watchObject.stopwatchGMTdelta)")
            stopwatchTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.stopwatchChanged), userInfo: nil, repeats: true)
            stopwatchTimer?.fire()
        } else {
            watchObject.stopwatchPaused = true
            stopwatchTimer?.invalidate()
        }
        watchVC?.StopflickerColon()
    }
    
    @objc func stopwatchChanged() {
        updateStopwatchObject(delta: watchObject.stopwatchGMTdelta)
        updateStopwatchUI()
    }
    
    //Adding and subtructing in set mode (hour, min, sec) for clock and alarm modes//
    func addSubTime(up: Bool) {
        let multiplier = up ? 1 : -1
        if watchObject.currentMode == .alarm {
            switch watchObject.currentSetMode {
            case .none:
                updateAlarmStatus(on: up ? true : false)
            case .hour:
                if (multiplier == 1 && alarmObject.hour < 23) ||
                   (multiplier == -1 && alarmObject.hour > 0) {
                    alarmObject.hour += 1*multiplier
                } else {
                    alarmObject.hour = multiplier == 1 ? 0 : 23
                }
            case .min:
                if (multiplier == 1 && alarmObject.min < 59) ||
                   (multiplier == -1 && alarmObject.min > 0) {
                    alarmObject.min += 1*multiplier
                } else {
                    alarmObject.min = multiplier == 1 ? 0 : 59
                }
            case .sec:
                break
            }
            watchVC?.setTime(hour: alarmObject.hour, min: alarmObject.min)
        } else {
            switch watchObject.currentSetMode {
            case .none:
                break
            case .hour:
                watchObject.localGMTdelta += Double(60*60*multiplier)
            case .min:
                watchObject.localGMTdelta += Double(60*multiplier)
            case .sec:
                watchObject.localGMTdelta += TimeInterval(-timeObject.sec)
                break
            }
        }
    }
    
    func updateAlarmStatus(on: Bool) {
        alarmObject.alarmOn = on
        watchVC?.setAlarm(on: alarmObject.alarmOn)
    }
    
    func watchModeTapped() {
        if watchObject.currentSetMode == .none {
            watchVC?.StopflickerColon()
            stopAlarmSound(snooze: false)
            soundPlayer.playSound(type: .beep)
            
            if watchObject.currentMode == .clock {
                watchObject.currentMode = .alarm
            } else if watchObject.currentMode == .alarm {
                watchObject.currentMode = .stopwatch
            } else if watchObject.currentMode == .stopwatch {
                watchObject.currentMode = .clock
            }
            
            watchVC?.setMode(mode: watchObject.currentMode)
            
            switch watchObject.currentMode {
            case .alarm:
                self.watchVC?.setTime(hour: alarmObject.hour, min: alarmObject.min)
            case .clock:
                timeChanged()
            case .stopwatch:
                updateStopwatchUI()
            }
        }
    }
    
    func updateStopwatchUI() {
        if watchObject.currentMode == .stopwatch {
            if watchObject.stopwatchLap {
                if stopwatchObject.hour > 0 {
                    self.watchVC?.setTime(hour: stopwatchObject.lapHour, min: stopwatchObject.lapMin, sec: stopwatchObject.lapSec)
                } else {
                    self.watchVC?.setTime(hour: stopwatchObject.lapMin, min: stopwatchObject.lapSec, sec: stopwatchObject.lapMili)
                }
            } else {
                if stopwatchObject.hour > 0 {
                    self.watchVC?.setTime(hour: stopwatchObject.hour, min: stopwatchObject.min, sec: stopwatchObject.sec)
                } else {
                    self.watchVC?.setTime(hour: stopwatchObject.min, min: stopwatchObject.sec, sec: stopwatchObject.mili)
                }
            }
            if !watchObject.stopwatchPaused && (stopwatchObject.mili == 0 || stopwatchObject.mili == 50) {
                watchVC?.flickerColon()
            }
            watchVC?.setTitle(mode: watchObject.currentMode, additionalText: watchObject.stopwatchLap ? " - Lap" : "")
        }
    }
    
    func updateTimeObject(delta: TimeInterval) {
        let date = Date()
        let dateWithDelta = date.addingTimeInterval(delta)
        timeObject.hour = calendar.component(.hour, from: dateWithDelta)
        timeObject.min = calendar.component(.minute, from: dateWithDelta)
        timeObject.sec = calendar.component(.second, from: dateWithDelta)
        //print("\(timeObject.hour):\(timeObject.min):\(timeObject.sec)")
    }
    
    func updateStopwatchObject(delta: TimeInterval) {
        let date = Date()
        let dateWithDelta = date.addingTimeInterval(delta)
        stopwatchObject.hour = calendar.component(.hour, from: dateWithDelta)
        stopwatchObject.min = calendar.component(.minute, from: dateWithDelta)
        stopwatchObject.sec = calendar.component(.second, from: dateWithDelta)
        stopwatchObject.mili = calendar.component(.nanosecond, from: dateWithDelta)/10000000
        
        if !watchObject.stopwatchLap {
            stopwatchObject.lapHour = stopwatchObject.hour
            stopwatchObject.lapMin = stopwatchObject.min
            stopwatchObject.lapSec = stopwatchObject.sec
            stopwatchObject.lapMili = stopwatchObject.mili
        }
    }
    
    @objc func timeChanged() {
        if watchObject.currentMode == .clock {
            self.updateTimeObject(delta: watchObject.localGMTdelta)
            self.watchVC?.setTime(hour: timeObject.hour, min: timeObject.min, sec: timeObject.sec)
        }
        if let startSetPress = self.startSetPress, watchObject.currentMode != .stopwatch {
            if Int(Date().timeIntervalSince(startSetPress)) >= watchObject.buttonSetSec {
                self.startSetPress = nil
                if watchObject.currentSetMode == .none {
                    setCurrentSetMode(mode: .hour)
                } else {
                    setCurrentSetMode(mode: .none)
                }
            }
        }
        
        //Create the flicker on/off while setting digits//
        if watchObject.currentSetMode != .none {
            setCurrentSetMode(mode: watchObject.currentSetMode)
        }
        
        //Check if to start an alarm sound//
        if alarmObject.hour == timeObject.hour &&
            alarmObject.min == timeObject.min &&
            timeObject.sec == 0 {
            if alarmSoundSeconds == 0 {
                initializeAlarmSound()
            }
        }
    }
    
    @objc func initializeAlarmSound() {
        if alarmObject.alarmOn {
            alarmSoundSeconds = watchObject.alarmSoundSec
            alarmSound()
        }
    }
    
    @objc func alarmSound() {
        soundPlayer.playSound(type: .alarm)
        if alarmSoundSeconds > 0 {
            alarmSoundSeconds -= 1
            self.perform(#selector(self.alarmSound), with: nil, afterDelay: 1)
        }
    }
    
    func stopAlarmSound(snooze: Bool) {
        if alarmSoundSeconds > 0 {
            alarmSoundSeconds = 0
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.alarmSound), object: nil)
            soundPlayer.stop()
            
            //Start snooze option//
            if snooze {
                self.perform(#selector(self.initializeAlarmSound), with: nil, afterDelay: TimeInterval(watchObject.snoozeSec))
            }
        }
    }
    
    //Update UI with set time mode//
    func setCurrentSetMode(mode: SetMode) {
        watchObject.currentSetMode = mode
        watchVC?.setModeUpdateUI(mode: mode)
    }
}
