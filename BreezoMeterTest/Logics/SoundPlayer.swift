//
//  SoundPlayer.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import Foundation
import AVFoundation

enum SoundType: String {
    case beep = "beep"
    case alarm = "alarm"
}

class SoundPlayer: NSObject {
    var player: AVAudioPlayer?
    func playSound(type: SoundType) {
        DispatchQueue.main.async {
            guard let url = Bundle.main.url(forResource: type.rawValue, withExtension: "mp3") else { return }
            
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)
                
                /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
                self.player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
                
                guard let player = self.player else { return }
                
                player.play()
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func stop() {
        player?.stop()
        player = nil
    }
}
