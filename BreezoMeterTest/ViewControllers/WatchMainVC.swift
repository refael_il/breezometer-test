//
//  ViewController.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import UIKit

protocol WatchMainVCDelegate: AnyObject {
    func watchModeTapped()
    func WatchUpTapped()
    func watchDownTapped()
    func watchSetStartedTapped()
    func watchSetEndedTapped()
    func watchLightTapped()
}

class WatchMainVC: UIViewController {
    weak var delegate: WatchMainVCDelegate?
    
    @IBOutlet weak var stackTimeLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackTimeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackTimeWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stackTime: UIStackView!
    
    var originalTimeWidth: CGFloat = 0
    var originalTimeHeight: CGFloat = 0
    var originalTimeLeading: CGFloat = 0
    var originalScreenBackgroundColor: UIColor?
    
    @IBOutlet weak var viewScreen: UIView!
    
    @IBOutlet weak var ivAlarmIcon: UIImageView!
    @IBOutlet weak var lblColon: UILabel!
    @IBOutlet weak var lblMode: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewScreen.layer.cornerRadius = 5
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "WatchUILoaded"), object: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Save original values (assuming no other screens exist)//
        originalTimeWidth = stackTimeWidthConstraint.constant
        originalTimeHeight = stackTimeHeightConstraint.constant
        originalTimeLeading = stackTimeLeadingConstraint.constant
        originalScreenBackgroundColor = viewScreen.backgroundColor
    }

    @IBAction func lightTapped(_ sender: UIButton) {
        delegate?.watchLightTapped()
    }
    
    @IBAction func modeTapped(_ sender: UIButton) {
        delegate?.watchModeTapped()
    }
    
    @IBAction func setStarted(_ sender: UIButton) {
        delegate?.watchSetStartedTapped()
    }
    
    @IBAction func setEnded(_ sender: UIButton) {
        delegate?.watchSetEndedTapped()
    }
    
    @IBAction func upTapped(_ sender: UIButton) {
        delegate?.WatchUpTapped()
    }
    
    @IBAction func downTapped(_ sender: UIButton) {
        delegate?.watchDownTapped()
    }
    
    //Stopwatch Colon : flicker//
    func flickerColon() {
        lblColon.alpha = lblColon.alpha == 1 ? 0 : 1
    }
    
    //Stop Colon flicker//
    func StopflickerColon() {
        lblColon.alpha = 1
    }
    
    //Set time Flicker affect//
    func setModeUpdateUI(mode: SetMode) {
        switch mode {
        case .none:
            lblHours.alpha = 1
            lblMinutes.alpha = 1
            lblSeconds.alpha = 1
        case .hour:
            lblHours.alpha = lblHours.alpha == 0 ? 1 : 0
            lblMinutes.alpha = 1
            lblSeconds.alpha = 1
        case .min:
            lblHours.alpha = 1
            lblMinutes.alpha = lblMinutes.alpha == 0 ? 1 : 0
            lblSeconds.alpha = 1
        case .sec:
            lblHours.alpha = 1
            lblMinutes.alpha = 1
            lblSeconds.alpha = lblSeconds.alpha == 0 ? 1 : 0
        }
    }
    
    //Update shown time (Clock and Alarm modes)//
    func setTime(hour: Int, min: Int, sec: Int = 0) {
        lblHours.text = "\(getCleanDigits(digit: hour))"
        lblMinutes.text = "\(getCleanDigits(digit: min))"
        lblSeconds.text = "\(getCleanDigits(digit: sec))"
    }
    
    func getCleanDigits(digit: Int) -> String {
        return digit < 10 ? "0\(digit)" : "\(digit)"
    }
    
    //Move between Clock and Alarm modes//
    func setMode(mode: Mode) {
        var fontSize: CGFloat = 35
        var addedWidth: CGFloat = 0
        var addedHeight: CGFloat = 0
        var addedLeading: CGFloat = 0
        
        switch mode {
        case .alarm:
            fontSize = 40
            lblSeconds.isHidden = true
            addedWidth = 10
            addedHeight = 5
            addedLeading = 7
        case .clock:
            lblSeconds.isHidden = false
        case .stopwatch:
            lblSeconds.isHidden = false
        }
        
        lblMode.text = getTitleForCurrentMode(mode: mode)
        
        lblHours.font = lblHours.font.withSize(fontSize)
        lblMinutes.font = lblMinutes.font.withSize(fontSize)
        stackTime.layoutIfNeeded()
        stackTimeWidthConstraint.constant = originalTimeWidth + addedWidth
        stackTimeHeightConstraint.constant = originalTimeHeight + addedHeight
        stackTimeLeadingConstraint.constant = originalTimeLeading + addedLeading
    }
    
    func getTitleForCurrentMode(mode: Mode) -> String {
        switch mode {
        case .alarm:
            return "Alarm"
        case .clock:
            return "Clock"
        case .stopwatch:
            return "Stopwatch"
        }
    }
    
    //Add text to Title label//
    func setTitle(mode: Mode, additionalText: String) {
        lblMode.text = getTitleForCurrentMode(mode: mode) + "\(additionalText)"
    }
    
    //Update UI alarm on/off//
    func setAlarm(on: Bool) {
        ivAlarmIcon.isHidden = !on
    }
    
    //Turn on the light//
    @objc func light(on: Bool) {
        viewScreen.backgroundColor = on ? .green : originalScreenBackgroundColor
    }
}

