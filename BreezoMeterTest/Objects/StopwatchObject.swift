//
//  TimeObject.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import Foundation

class StopwatchObject: NSObject {
    var hour = 0
    var min = 0
    var sec = 0
    var mili = 0
    
    var lapHour = 0
    var lapMin = 0
    var lapSec = 0
    var lapMili = 0
    
    func reset() {
        hour = 0
        min = 0
        sec = 0
        mili = 0
        lapHour = 0
        lapMin = 0
        lapSec = 0
        lapMili = 0
    }
}
