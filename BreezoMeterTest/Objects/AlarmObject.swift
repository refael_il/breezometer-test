//
//  AlarmObject.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import Foundation

class AlarmObject: NSObject {
    var hour = 12
    var min = 0
    var alarmOn = false
}
