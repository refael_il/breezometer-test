//
//  TimeObject.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import Foundation

class TimeObject: NSObject {
    var hour = 0
    var min = 0
    var sec = 0
}
