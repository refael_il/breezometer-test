//
//  WatchObject.swift
//  BreezoMeterTest
//
//  Created by Refael Sommer on 26/05/2021.
//

import Foundation

class WatchObject: NSObject {
    let buttonSetSec = 2
    let backlightSec = 5
    let alarmSoundSec = 20
    let snoozeSec = 4
    var localGMTdelta: TimeInterval = 0
    var stopwatchGMTdelta: TimeInterval = 0
    var stopwatchPaused: Bool = true
    var stopwatchLap: Bool = false
    var currentMode: Mode = .clock
    var currentSetMode: SetMode = .none
}
