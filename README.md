Running the app is as simple as just running it straight from xcode to the simulator or signing it and running it on a device, no pods needed.

In this session we will practice joint work by developing a small (but proud!) application,
according to a given spec.

Part 1: Spec (10m)
Before we start the frontal session, please read the spec carefully

Part 2: Design (50m)
Sketch a design that describes the different components and the relations between them

● We suggest using Miro, but any other platform will do (If you choose Miro, make sure to
sign-up before the session, in order to save time).

Part 3: Implementation (5h)
Implement the suggested design

● Please don’t exceed the time frame - you are not being tested on speed or quantity

● Comments / TODOs are welcome

Part 4: Review (30m)
A short discussion on the implementation and summary

Notes:

● The exercise is meant to get the notion of working together, to see your coding style,
design and mind set.

● We are looking for a rough UI,some implemented features but a well designed code and
architecture - you are NOT expected to present a beautiful, fully featured and well
debugged application.

● Please stick to the time frame - we don’t want to waste your time
GOOD LUCK AND HAPPY CODING!

Congratulations on your purchase of our Fancy Digital Alarm Clock (F-DAC)!
Features of F-DAC include:

● LED Backlight for night viewing

● Single Alarm with Snooze function

1. CLOCK FUNCTION
- Press and hold for 2 seconds the SET button to enter the clock setting mode
- Press SET to toggle among the following displays: hour > minute > second
- Press and hold for 2 seconds the SET button to exit the clock setting mode

2. ALARM FUNCTION
- Press “Mode” to tuggle Clock View mode <-> Alarm View mode
- On alarm mode: Press up/down buttons to Activate/deactivate the alarm scheduler
- Press and hold for 2 seconds the SET button to enter the alarm setting mode
- Press SET to toggle among the following displays: hour > minute
- Press and hold for 2 seconds the SET button to exit the alarm setting mode

3. SNOOZE-LIGHT BUTTON
- Press to turn on the backlight for 5 seconds
- When the alarm rings, Press to activate the 5-minute Snooze function when the alarm
goes off. Any other clock will stop the alarm till next day

I have added the stopper for fun :)
